<?php

// Plugin definition
$plugin = array(
  'title' => t('Flex960'),
  'category' => t('Builders'),
  'icon' => 'flexible.png',

  'theme' => 'flex960',
  'admin theme' => 'flex960_admin',
  'admin css' => 'flex960-admin.css',

  'settings form' => 'flex960_settings_form',
  'settings submit' => 'flex960_settings_submit',
  'settings validate' => 'flex960_settings_validate',
  'panels function' => 'flex960_panels',
  'hook menu' => 'flex960_menu',

  'ajax' => array(
    'settings' => 'panels_ajax_flex960_edit_settings',
    'add' => 'panels_ajax_flex960_edit_add',
    'remove' => 'panels_ajax_flex960_edit_remove',
  ),
);

/**
 * Provide defaults for empty settings.
 */
function flex960_default_settings(&$settings, &$layout) {
  if (empty($settings)) {
    $settings = array(
      'items' => array(
        'canvas' => array(
          'type' => 'row',
          'contains' => 'column',
          'grid_container' => 16,
          'children' => array('main'),
          'parent' => NULL,
        ),
        'main' => array(
          'type' => 'column',
          'grid' => 16,
          'prefix' => 0,
          'suffix' => 0,
          'push' => 0,
          'pull' => 0,
          'children' => array('main-row'),
          'parent' => 'canvas',
        ),
        'main-row' => array(
          'type' => 'row',
          'contains' => 'region',
          'children' => array('center'),
          'parent' => 'main',
        ),
        'center' => array(
          'type' => 'region',
          'title' => t('Center'),
          'parent' => 'main-row',
        ),
      ),
    );
  }
}

/**
 * Define the actual list of columns and rows for this flexible panel.
 */
function flex960_panels($display, $settings, $layout) {
  $items = array();
  flex960_default_settings($settings, $layout);
  foreach ($settings['items'] as $id => $item) {
    if ($item['type'] == 'region') {
      $items[$id] = $item['title'];
    }
  }
  return $items;
}

/**
 * Create a renderer object.
 */
function flex960_create_renderer($admin, $id, $content, $settings, &$display, $layout, $handler) {
  $renderer = new stdClass;
  $renderer->settings = $settings;
  $renderer->content = $content;
  $renderer->css_id = $id;
  $renderer->did = &$display->did;
  $renderer->id_str = $id ? ' id="' . $id . '"' : '';
  $renderer->admin = $admin;
  $renderer->handler = $handler;

  // Set up basic classes for all of our components.
  $renderer->name = !empty($layout['layout']) ? $layout['layout']->name : $display->did;
  $renderer->item_class['canvas'] = '';
  $renderer->item_class['column'] = 'panels-flex960-column';
  $renderer->item_class['row'] = 'panels-flex960-row';
  $renderer->item_class['region'] = 'panels-flex960-region';

  // Override these if selected from the UI and not in admin mode.
  if (!$admin) {
    if (!empty($settings['items']['canvas']['class'])) {
      $renderer->item_class['canvas'] = $settings['items']['canvas']['class'];
    }
    if (!empty($settings['items']['canvas']['column_class'])) {
      $renderer->item_class['column'] = $settings['items']['canvas']['column_class'];
    }
    if (!empty($settings['items']['canvas']['row_class'])) {
      $renderer->item_class['row'] = $settings['items']['canvas']['row_class'];
    }
    if (!empty($settings['items']['canvas']['region_class'])) {
      $renderer->item_class['region'] = $settings['items']['canvas']['region_class'];
    }
  }

  // Make some grid classes so it's easier to reference them.
  $renderer->grid = _flex960_create_grid_classes($settings['items'], $admin);

  // Return the renderer object.
  return $renderer;
}

/**
 * Create grid classes.
 */
function _flex960_create_grid_classes($items, $admin = FALSE) {
  $grid_classes = array();
  if ($admin) {
    $items['canvas']['canvas_class_pattern'] = 'container_%d';
  }
  $grid_classes['canvas'] = strtr($items['canvas']['canvas_class_pattern'], array('%d' => $items['canvas']['grid_container']));
  foreach ($items as $key => $item) {
    if ($item['type'] == 'column') {
      if ($admin) {
        $item['grid_class_pattern'] = 'grid_%d';
        $item['prefix_class_pattern'] = 'prefix_%d';
        $item['suffix_class_pattern'] = 'suffix_%d';
        $item['push_class_pattern'] = 'push_%d';
        $item['pull_class_pattern'] = 'pull_%d';
      }
      $grid = array();
      if (!empty($item['grid'])) {
        $grid[] = strtr($item['grid_class_pattern'], array('%d' => $item['grid']));
      }
      if (!empty($item['grid_prefix'])) {
        $grid[] = strtr($item['prefix_class_pattern'], array('%d' => $item['grid_prefix']));
      }
      if (!empty($item['grid_suffix'])) {
        $grid[] = strtr($item['suffix_class_pattern'], array('%d' => $item['grid_suffix']));
      }
      if (!empty($item['grid_push'])) {
        $grid[] = strtr($item['push_class_pattern'], array('%d' => $item['grid_push']));
      }
      if (!empty($item['grid_pull'])) {
        $grid[] = strtr($item['pull_class_pattern'], array('%d' => $item['grid_pull']));
      }
      $grid_classes[$key] = trim(implode(' ', $grid));
    }
  }
  return $grid_classes;
}

/**
 * Draw the flex960 layout, starting at canvas.
 */
function theme_flex960($id, $content, $settings, $display, $layout, $handler, $admin = FALSE) {
  flex960_default_settings($settings, $layout);

  // Add the grid framework if the theme doesn't provide its own.
  // Also add the grid when in admin.
  if (!empty($admin) || empty($settings['items']['canvas']['theme_provided'])) {
    drupal_add_css(drupal_get_path('module', 'flex960') . '/framework/code/css/960.css');
  }

  $renderer = flex960_create_renderer($admin, $id, $content, $settings, $display, $layout, $handler);
  $output = "<div class=\"panels-flex960 {$renderer->grid['canvas']}\"$renderer->id_str>\n";
  if (!empty($admin)) {
    // Add the item links (add, remove, etc.) when viewed in admin mode.
    $output .= flex960_render_item_links($renderer, 'canvas', $settings['items']['canvas']);
  }
  // Render all the children items from canvas.
  $output .= flex960_render_children($renderer, $settings['items']['canvas']['children']);
  $output .= "<div class=\"panels-flex960 clear\"></div>\n";
  $output .= "</div>\n";

  return $output;
}

/**
 * Draw the flex960 admin layout, starting at canvas.
 */
function theme_flex960_admin($id, $content, $settings, $display, $layout, $handler) {
  return theme_flex960($id, $content, $settings, $display, $layout, $handler, TRUE);
}

/**
 * Render all children for this item.
 */
function flex960_render_children($renderer, $items) {
  $max = count($list) - 1;
  $output = '';
  foreach ($items as $position => $id) {
    $item = $renderer->settings['items'][$id];
    switch ($item['type']) {
      case 'column':
        $content = flex960_render_children($renderer, $item['children']);
        $output .= flex960_render_item($renderer, $item, $content, $id, $position, $max);
        break;
      case 'row':
        $content = flex960_render_children($renderer, $item['children']);
        $output .= flex960_render_item($renderer, $item, $content, $id, $position, $max);
        break;
      case 'region':
        $content = isset($renderer->content[$id]) ? $renderer->content[$id] : '&nbsp;';
        $output .= flex960_render_item($renderer, $item, $content, $id, $position, $max);
        break;
    }
  }
  return $output;
}

/**
 * Render a single item in the layout.
 */
function flex960_render_item($renderer, $item, $content, $id, $position, $max) {
  $item_class = _flex960_create_item_classes($renderer, $id, $item);
  $output = '<div class="' . trim(implode(' ', $item_class)) . '">';
  if (!empty($renderer->admin)) {
    // Add the item links (add, remove, etc.) when viewed in admin mode.
    $output .= flex960_render_item_links($renderer, $id, $item);
  }
  if ($item['type'] == 'region' && empty($content)) {
    $output .= '&nbsp;';
  }
  else {
    $output .= $content;
  }
  $output .= "</div>\n";
  return $output;
}

/**
 * Create item classes.
 */
function _flex960_create_item_classes($renderer, $id, $item) {
  $base = $renderer->item_class[$item['type']];
  $item_class = array($base);
  if ($item['type'] == 'column') {
    if (!empty($renderer->grid[$id])) {
      $item_class[] = $renderer->grid[$id];
    }
    elseif (!empty($renderer->admin)) {
      $item_class[] = 'grid_2';
    }
  }
  if ($position == 0) {
    if ($item['parent'] != 'canvas') {
      $item_class[] = "alpha";
    }
    $item_class[] = "first";
  }
  if ($position == $max) {
    if ($item['parent'] != 'canvas') {
      $item_class[] = "omega";
    }
    $item_class[] = "last";
  }
  if (isset($item['class'])) {
    $item_class[] = check_plain($item['class']);
  }
  return $item_class;
}

/**
 * Render the dropdown links for an item.
 */
function flex960_render_item_links($renderer, $id, $item) {
  $links = array();
  $remove = '';
  $add = '';
  if ($item['type'] == 'column') {
    $title = t('Column');
    $settings = t('Column settings');
    if (empty($item['children'])) {
      $remove = t('Remove column');
      $add = t('Add row');
    }
    else {
      $add = t('Add row to top');
      $add2 = t('Add row to bottom');
    }
  }
  else if ($item['type'] == 'row') {
    if ($id == 'canvas') {
      $title = t('Canvas');
      $settings = t('Canvas settings');
    }
    else {
      $title = t('Row');
      $settings = t('Row settings');
    }
    if (empty($item['children'])) {
      if ($id != 'canvas') {
        $remove = t('Remove row');
      }
      $add = $item['contains'] == 'region' ? t('Add region') : t('Add column');
    }
    else {
      $add = $item['contains'] == 'region' ? t('Add region to left') : t('Add column to left');
      $add2 = $item['contains'] == 'region' ? t('Add region to right') : t('Add column to right');
    }
  }
  else if ($item['type'] == 'region') {
    $title = t('Region');
    $settings = t('Region settings');
    $remove = t('Remove region');
  }

  if (!empty($settings)) {
    $links[] = array(
      'title' => $settings,
      'href' => $renderer->handler->get_url('layout', 'settings', $id),
      'attributes' => array('class' => 'ctools-use-modal'),
    );
  }
  if ($add) {
    $links[] = array(
      'title' => $add,
      'href' => $renderer->handler->get_url('layout', 'add', $id),
      'attributes' => array('class' => 'ctools-use-modal'),
    );
  }
  if (isset($add2)) {
    $links[] = array(
      'title' => $add2,
      'href' => $renderer->handler->get_url('layout', 'add', $id, 'right'),
      'attributes' => array('class' => 'ctools-use-modal'),
    );
  }
  if ($remove) {
    $links[] = array(
      'title' => $remove,
      'href' => $renderer->handler->get_url('layout', 'remove', $id),
      'attributes' => array('class' => 'ctools-use-ajax'),
    );
  }

  return theme('ctools_dropdown', $title, $links, FALSE, 'flex960-dropdown flex960-dropdown-' . $id);
}

/**
 * Helper function to refresh the admin interface after a change.
 */
function _flex960_ajax_admin_refresh(&$handler) {
  panels_edit_cache_set($handler->cache);
  $output = array();
  $output[] = ctools_ajax_command_replace('#panels-dnd-main',  $handler->render());
  $output[] = ctools_modal_command_dismiss();
  return $output;
}

/**
 * AJAX responder to edit settings for an item.
 */
function panels_ajax_flex960_edit_settings($handler, $id) {
  $settings = &$handler->display->layout_settings;
  flex960_default_settings($settings, $handler->plugins['layout']);

  if (empty($settings['items'][$id])) {
    ctools_modal_render(t('Error'), t('Invalid item id.'));
  }

  $item = &$settings['items'][$id];
  $siblings = array();

  if ($id != 'canvas') {
    $siblings = $settings['items'][$item['parent']]['children'];
  }

  switch ($item['type']) {
    case 'column':
      $title = t('Configure column');
      break;
    case 'row':
      if ($id == 'canvas') {
        $title = t('Configure canvas');
      }
      else {
        $title = t('Configure row');
      }
      break;
    case 'region':
      $title = t('Configure region');
      break;
  }

  $form_state = array(
    'display' => &$handler->display,
    'item' => &$item,
    'id' => $id,
    'siblings' => $siblings,
    'settings' => &$settings,
    'ajax' => TRUE,
    'title' => $title,
    'op' => 'edit',
  );

  $output = ctools_modal_form_wrapper('flex960_config_item_form', $form_state);
  if (empty($output)) {
    $output = _flex960_ajax_admin_refresh($handler);
  }
  $handler->commands = $output;
}

/**
 * Configure a row, column or region.
 */
function flex960_config_item_form(&$form_state, $add_form = FALSE) {
  $display = &$form_state['display'];
  $item = &$form_state['item'];
  $siblings = &$form_state['siblings'];
  $settings = &$form_state['settings'];
  $id = &$form_state['id'];

  if ($item['type'] == 'region') {
    $form['title'] = array(
      '#title' => t('Region title'),
      '#type' => 'textfield',
      '#default_value' => $item['title'],
      '#required' => TRUE,
    );
  }

  if ($id == 'canvas' && !$add_form) {
    $form['theme_provided'] = array(
      '#type' => 'checkbox',
      '#title' => t('Theme provides grid framework'),
      '#description' => t('If the site theme layout will be utilizing a grid framework, check this box to prevent this layout from including its own stylesheet.'),
      '#default_value' => isset($item['theme_provided']) ? $item['theme_provided'] : TRUE,
    );
    $form['grid_container'] = array(
      '#type' => 'select',
      '#title' => t('Grid container dimensions'),
      '#options' => array(
        '12' => t('12-column grid'),
        '16' => t('16-column grid'),
      ),
      '#default_value' => isset($item['grid_container']) ? $item['grid_container'] : '16',
    );
    $form['canvas_class_pattern'] = array(
      '#type' => 'textfield',
      '#title' => t('Canvas class pattern'),
      '#description' => t('The pattern applied to the class of the canvas. Substitute %d for the container dimensions.'),
      '#default_value' => isset($item['canvas_class_pattern']) ? $item['canvas_class_pattern'] : 'container-%d',
    );
    $form['grid_class_pattern'] = array(
      '#type' => 'textfield',
      '#title' => t('Grid width pattern'),
      '#description' => t('The pattern applied to the class of the column. Substitute %d for the grid width value.'),
      '#default_value' => isset($item['grid_class_pattern']) ? $item['grid_class_pattern'] : 'grid-%d',
    );
    $form['prefix_class_pattern'] = array(
      '#type' => 'textfield',
      '#title' => t('Grid prefix pattern'),
      '#description' => t('The pattern applied to the class of the column. Substitute %d for the grid prefix value.'),
      '#default_value' => isset($item['prefix_class_pattern']) ? $item['prefix_class_pattern'] : 'prefix-%d',
    );
    $form['suffix_class_pattern'] = array(
      '#type' => 'textfield',
      '#title' => t('Grid suffix pattern'),
      '#description' => t('The pattern applied to the class of the column. Substitute %d for the grid suffix value.'),
      '#default_value' => isset($item['suffix_class_pattern']) ? $item['suffix_class_pattern'] : 'suffix-%d',
    );
    $form['push_class_pattern'] = array(
      '#type' => 'textfield',
      '#title' => t('Grid push pattern'),
      '#description' => t('The pattern applied to the class of the column. Substitute %d for the grid push value.'),
      '#default_value' => isset($item['push_class_pattern']) ? $item['push_class_pattern'] : 'push-%d',
    );
    $form['pull_class_pattern'] = array(
      '#type' => 'textfield',
      '#title' => t('Grid pull pattern'),
      '#description' => t('The pattern applied to the class of the column. Substitute %d for the grid pull value.'),
      '#default_value' => isset($item['pull_class_pattern']) ? $item['pull_class_pattern'] : 'pull-%d',
    );
    $form['classes'] = array(
      '#type' => 'fieldset',
      '#title' => t('Classes'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#tree' => FALSE,
    );
    $form['classes']['class'] = array(
      '#title' => t('Canvas class'),
      '#type' => 'textfield',
      '#default_value' => isset($item['class']) ? $item['class'] : '',
      '#description' => t('This class will the primary class for this layout. It will also be appended to all column, row and region_classes to ensure that layouts within layouts will not inherit CSS from each other. If left blank, the name of the layout or ID of the display will be used.'),
    );
    $form['classes']['column_class'] = array(
      '#title' => t('Column class'),
      '#type' => 'textfield',
      '#default_value' => isset($item['column_class']) ? $item['column_class'] : '',
      '#description' => t('This class will be applied to all columns of the layout. If left blank this will be panels-flex960-column.'),
    );
    $form['classes']['row_class'] = array(
      '#title' => t('Row class'),
      '#type' => 'textfield',
      '#default_value' => isset($item['row_class']) ? $item['row_class'] : '',
      '#description' => t('This class will be applied to all rows of the layout. If left blank this will be panels-flex960-row.'),
    );
    $form['classes']['region_class'] = array(
      '#title' => t('Region class'),
      '#type' => 'textfield',
      '#default_value' => isset($item['region_class']) ? $item['region_class'] : '',
      '#description' => t('This class will be applied to all regions of the layout. If left blank this will be panels-flex960-region.'),
    );
  }
  elseif ($item['type'] == 'column') {
    $container16 = $display->layout_settings['items']['canvas']['grid_container'] == '16';
    $form['grid'] = array(
      '#type' => 'select',
      '#title' => t('Grid width'),
      '#options' => ($container16) ? range(0, 16) : range(0, 12),
      '#default_value' => isset($item['grid']) ? $item['grid'] : 0,
    );
    $form['grid_prefix'] = array(
      '#type' => 'select',
      '#title' => t('Grid prefix'),
      '#options' => ($container16) ? range(0, 16) : range(0, 12),
      '#default_value' => isset($item['grid_prefix']) ? $item['grid_prefix'] : 0,
    );
    $form['grid_suffix'] = array(
      '#type' => 'select',
      '#title' => t('Grid suffix'),
      '#options' => ($container16) ? range(0, 16) : range(0, 12),
      '#default_value' => isset($item['grid_suffix']) ? $item['grid_suffix'] : 0,
    );
    $form['grid_push'] = array(
      '#type' => 'select',
      '#title' => t('Grid push'),
      '#options' => ($container16) ? range(0, 16) : range(0, 12),
      '#default_value' => isset($item['grid_push']) ? $item['grid_push'] : 0,
    );
    $form['grid_pull'] = array(
      '#type' => 'select',
      '#title' => t('Grid pull'),
      '#options' => ($container16) ? range(0, 16) : range(0, 12),
      '#default_value' => isset($item['grid_pull']) ? $item['grid_pull'] : 0,
    );
  }
  elseif ($item['type'] == 'row') {
    $form['contains'] = array(
      '#type' => 'select',
      '#title' => t('Contains'),
      '#default_value' => $item['contains'],
      '#options' => array(
        'region' => t('Regions'),
        'column' => t('Columns'),
      ),
    );
    if (!empty($item['children'])) {
      $form['contains']['#disabled'] = TRUE;
      $form['contains']['#value'] = $item['contains'];
      $form['contains']['#description'] = t('You must remove contained items to change the row container type.');
    }

    $form['classes'] = array(
      '#type' => 'fieldset',
      '#title' => t('Classes'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#tree' => FALSE,
    );
    $form['classes']['class'] = array(
      '#title' => t('CSS class'),
      '#type' => 'textfield',
      '#default_value' => isset($item['class']) ? $item['class'] : '',
      '#description' => t('Enter a CSS class that will be used. This can be used to apply automatic styling from your theme, for example.'),
    );
  }

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit handler for editing a flexible item.
 */
function flex960_config_item_form_submit(&$form, &$form_state) {
  $item = &$form_state['item'];
  if ($item['type'] == 'region') {
    $item['title'] = $form_state['values']['title'];
  }

  $item['class'] = $form_state['values']['class'];

  if ($form_state['id'] == 'canvas') {
    $item['theme_provided'] = $form_state['values']['theme_provided'];
    $item['grid_container'] = $form_state['values']['grid_container'];
    $item['canvas_class_pattern'] = $form_state['values']['canvas_class_pattern'];
    $item['grid_class_pattern'] = $form_state['values']['grid_class_pattern'];
    $item['prefix_class_pattern'] = $form_state['values']['prefix_class_pattern'];
    $item['suffix_class_pattern'] = $form_state['values']['suffix_class_pattern'];
    $item['push_class_pattern'] = $form_state['values']['push_class_pattern'];
    $item['pull_class_pattern'] = $form_state['values']['pull_class_pattern'];
    $item['column_class'] = $form_state['values']['column_class'];
    $item['row_class'] = $form_state['values']['row_class'];
    $item['region_class'] = $form_state['values']['region_class'];
  }
  else if ($item['type'] != 'row') {
    $item['grid'] = $form_state['values']['grid'];
    $item['grid_prefix'] = $form_state['values']['grid_prefix'];
    $item['grid_suffix'] = $form_state['values']['grid_suffix'];
    $item['grid_push'] = $form_state['values']['grid_push'];
    $item['grid_pull'] = $form_state['values']['grid_pull'];
  }
  else {
    $item['contains'] = $form_state['values']['contains'];
  }
}

/**
 * AJAX responder to add a new row, column or region.
 */
function panels_ajax_flex960_edit_add($handler, $id, $location = 'left') {
  $settings = &$handler->display->layout_settings;
  flex960_default_settings($settings, $handler->plugins['layout']);

  if (empty($settings['items'][$id])) {
    ctools_modal_render(t('Error'), t('Invalid item id.'));
  }

  $parent = &$settings['items'][$id];

  switch ($parent['type']) {
    case 'column':
      $title = t('Add row');
      // Create the new item with defaults.
      $item = array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(),
        'parent' => $id,
      );
      break;
    case 'row':
      switch ($parent['contains']) {
        case 'region':
          $title = $location == 'left' ? t('Add region to left') : t('Add region to right');
          $item = array(
            'type' => 'region',
            'title' => '',
            'parent' => $id,
          );
          break;
        case 'column':
          $title = $location == 'left' ? t('Add column to left') : t('Add column to right');
          $item = array(
            'type' => 'column',
            'parent' => $id,
            'children' => array(),
          );
          break;
      }
      break;
    case 'region':
      break;
  }

  $form_state = array(
    'display' => &$handler->display,
    'parent' => &$parent,
    'item' => &$item,
    'id' => $id,
    'settings' => &$settings,
    'ajax' => TRUE,
    'title' => $title,
    'location' => $location,
  );

  $output = ctools_modal_form_wrapper('flex960_add_item_form', $form_state);
  if (empty($output)) {
    $output = _flex960_ajax_admin_refresh($handler);
  }
  $handler->commands = $output;
}

/**
 * Form to add a row, column or region.
 */
function flex960_add_item_form(&$form_state) {
  return flex960_config_item_form($form_state, TRUE);
}

/**
 * Submit handler for adding a new item.
 */
function flex960_add_item_form_submit(&$form, &$form_state) {
  $item = &$form_state['item'];
  $parent = &$form_state['parent'];
  $location = &$form_state['location'];
  $settings = &$form_state['settings'];

  $item['class'] = $form_state['values']['class'];

  if ($item['type'] == 'region') {
    $item['title'] = $form_state['values']['title'];
  }

  if ($item['type'] == 'column') {
    $item['grid'] = $form_state['values']['grid'];
    $item['grid_prefix'] = $form_state['values']['grid_prefix'];
    $item['grid_suffix'] = $form_state['values']['grid_suffix'];
    $item['grid_push'] = $form_state['values']['grid_push'];
    $item['grid_pull'] = $form_state['values']['grid_pull'];
  }
  elseif ($item['type'] == 'row') {
    $item['contains'] = $form_state['values']['contains'];
  }

  if ($item['type'] == 'region') {
    // derive the region key from the title
    $key = preg_replace("/[^a-z0-9]/", '_', drupal_strtolower($item['title']));
    while (isset($settings['items'][$key])) {
      $key .= '_';
    }
    $form_state['key'] = $key;
  }
  else {
    $form_state['key'] = $key = max(array_keys($settings['items'])) + 1;
  }

  // Place the item.
  $settings['items'][$key] = $item;
  if ($location == 'left') {
    array_unshift($parent['children'], $key);
  }
  else {
    $parent['children'][] = $key;
  }
}

/**
 * AJAX responder to remove an existing row, column or region.
 */
function panels_ajax_flex960_edit_remove($handler, $id) {
  $settings = &$handler->display->layout_settings;
  flex960_default_settings($settings, $handler->plugins['layout']);

  if (empty($settings['items'][$id])) {
    ctools_ajax_render_error(t('Invalid item id.'));
  }

  // Find the offset of our array. This will also be the key because
  // this is a simple array.
  $item = &$settings['items'][$id];
  $siblings = &$settings['items'][$item['parent']]['children'];
  $offset = array_search($id, $siblings);
  // Remove the item.
  array_splice($siblings, $offset, 1);
  unset($settings['items'][$id]);

  // Save our new state.
  panels_edit_cache_set($handler->cache);

  // Refresh the admin UI.
  $output = _flex960_ajax_admin_refresh($handler);
  $handler->commands = $output;
}
